---
redirect_to: './billing_invoicing_payments.html#cancellations-downgrades-and-refunds'
---

This document was moved to [another location](./billing_invoicing_payments.html#cancellations-downgrades-and-refunds).

<!-- This redirect file can be deleted after <2021-04-12>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
